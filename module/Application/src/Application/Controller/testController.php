<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Libaries\Test;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Application\Models\Users;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\AvailableSpaceCapableInterface;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\TotalSpaceCapableInterface;

class TestController extends AbstractActionController
{
    public function __construct()
    {
        $this->cacheTime = 36000;
        $this->now = date("Y-m-d H:i:s");
        //$this->config = include __DIR__ . '../../../../config/module.config.php';
    }

    public function indexAction() 
    {
        echo json_encode($this->calculate(3,2,5));
        return $this->response;
    }

    private function calculate(int $startNum,int $incrementNum,int $length){
        $arrNum = array($startNum);
        $nextNum = $startNum;
        for($i = 1; $i<$length;$i++){
            $nextNum +=$i*$incrementNum;
            $arrNum[] =$nextNum;
        }
        return $arrNum;
    }

    public function mapAction(){
        $apikey = "AIzaSyD4WUXUk6HffLbsbQv8y6k9x2ch_naPo8k";
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=13.8212805,100.5174634&radius=1500&type=restaurant&keyword=Bangsue&key=$apikey"); 
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        $json= curl_exec($ch);
        echo $json;
        return $this->response;
    }
}